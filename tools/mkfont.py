#!/usr/bin/python3

import sys, getopt, yaml

from PIL import Image,  ImageFont, ImageDraw


def encode_char(char, fname, fsize, ordnum=None):

    img = Image.new('L', (200, 200), color=0)
    img.putpalette([0,0,0, 255,255,255])
    draw = ImageDraw.Draw(img)
    font = ImageFont.truetype(fname, fsize)

    draw.text((0,0), char, font=font, fill=1)
    area = img.getbbox()
    if area != None:
        x_offset, y_offset, width, height = area
    else: 
        x_offset, y_offset, width, height = 0, 0, 0, 0

    x_delta, _ = draw.textsize(char, font=font)
    width -= x_offset
    height -= y_offset

    size, rest = divmod(width*height, 8)
    size += 1 if rest else 0

    data = bytearray(size+6)
    data[0:6] = [ordnum if ordnum else ord(char), y_offset, width, height, x_offset, x_delta ]
    if width*height:
        index = 5
        mask = 0x80 
        for y in range(y_offset, height+y_offset):
            for x in range(x_offset, x_offset+width):
                if not (( (x-x_offset) + ((y-y_offset) * width)) % 8 ):
                    mask = 0x80
                    index += 1
                if img.getpixel((x,y)):
                    data[index] = data[index] | mask
                mask = mask >> 1
    else:
        ### y_offset correction for "empty" characer like ' '
        data[1] = fsize

    return data


### Main

if __name__ == '__main__':

    idx = 1
    if 'python3' in sys.argv[0]:
        idx = 2

    try:
        opts, args = getopt.getopt(sys.argv[idx:], "hc:d:", ["help", "config=", "dest="])
    except getopt.GetoptError:
        print ('mkfont.py -c <config.yml> -d <out_dir>')
        sys.exit(2)

    dest = './'

    for opt, arg in opts:
        if opt == ('-h', '--help'):
            print('mkfont.py -c <config.yml> -d <out_dir>')
            sys.exit()
        elif opt in ("-c", "--config"):
            cfgfile = arg
        elif opt in ("-d", "--dest"):
            dest = arg

    f = open(cfgfile, 'r')
    conf = yaml.safe_load(f)
    f.close()

    for fnt in conf['fonts']:

        font_data = []  
        optimize_h = fnt['y_shift'] if 'y_shift' in fnt.keys() else 0
        min_y_offset = fnt['size']

        ### Not specified characters, all visible will be added ord(32) to ord(254)
        if not 'chars' in fnt.keys():
            fnt['chars'] = []
            for c in range(32,255):
                fnt['chars'].append(chr(c))

        ### generate all characters in input char set
        for c in fnt['chars']:
            ### ugly ord(c[-1]) construction is when I need overload character in font set alphabet
            ### For exapmle, I need 'ř', which has ord 345, but I do not need '>', then in config will be 'ř>'
            ### And when in code will be tft.text('>epa'), on display you will see 'řepa'
            font_data.append(encode_char(c, fnt['file'], fnt['size'], ordnum=ord(c[-1]) ))
            min_y_offset = min(min_y_offset, font_data[-1][1])

        ### minimize font height to height of heightest character
        if optimize_h:
            for i in range(len(font_data)):
                font_data[i][1] -= min_y_offset if optimize_h == -255 else optimize_h

        of = open("{}/{}.py".format(dest,fnt['ttf_font']), 'w')
        #write header
        of.write("""_font = \\
    b'\\x00\\x00\\x{:02x}\\x00'""".format(fnt['size']))
        for char in font_data:
            #write char
            of.write("""
    b'""")
            for n in char:
                of.write("\\x{:02x}".format(n))
            of.write("""'""")
        #write footer
        of.write("""

    mvfont = memoryview(_font)

    """)
        of.close()

        print("{}/{}.py".format(dest,fnt['ttf_font']))



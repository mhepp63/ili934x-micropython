#!/usr/bin/python3

import sys
import getopt



def get_font_index(font):
    index = 4 #pos of first char
    while font[index] != 0xFF:
        d,m = divmod(font[index+2]*font[index+3], 8)
        chr_size = d+1 if m else d
        yield index
        index = index + 6 + chr_size


inputfile = ''
outputfile = ''

idx = 1
if 'python3' in sys.argv[0]:
    idx = 2

try:
    opts, args = getopt.getopt(sys.argv[idx:], "hi:o:", ["in=","out="])

except getopt.GetoptError:
    print ('c2py_font_gen.py -i <inputfile> -o <outputfile>')
    sys.exit(2)


for opt, arg in opts:
    if opt == '-h':
        print ('c2py_font_gen.py -i <inputfile> -o <outputfile>')
        sys.exit()
    elif opt in ("-i", "--in"):
        inputfile = arg
    elif opt in ("-o", "--out"):
        outputfile = arg


ff = open(inputfile, 'rb')

data = []

comment = False
comm_bg = False
in_data = False
had_data = False

for i,b in enumerate(ff.read()):
    
    if comment:
        if b == 10:
            comment = False
        continue

    if b == 47:
        if comm_bg:
            comment = True
            comm_bg = False
        else:
            comm_bg = True
        continue

    if b == 123:
        in_data = True
        continue

    if b == 125:
        had_data = True
        in_data = False
        break

    if b > 32 and in_data:
        data.append(chr(b))

ff.close()

data_int = [ int(item,0) for item in ''.join(data).split(',')]

idx = list(get_font_index(data_int))
idx.append(len(data_int)-1)

of = open(outputfile, 'w')

of.write("""_font = \\
b'""")
for i,n in enumerate(data_int):
    if i in idx:
        of.write("""' \\
b'""")
    of.write("\\x{:02x}".format(n))

of.write("""'

mvfont = memoryview(_font)

""")

of.close()


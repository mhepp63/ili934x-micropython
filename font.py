
def get_font_chars(font):
    index = 4 #pos of first char
    while font.mvfont[index] != 0xFF:
        d,m = divmod(font.mvfont[index+2]*font.mvfont[index+3], 8)
        chr_size = d+1 if m else d
        yield chr(font.mvfont[index])
        index = index + 6 + chr_size


def get_font_index(font):
    index = 4 #pos of first char
    while font.mvfont[index] != 0xFF:
        d,m = divmod(font.mvfont[index+2]*font.mvfont[index+3], 8)
        chr_size = d+1 if m else d
        yield (chr(font.mvfont[index]), index)
        index = index + 6 + chr_size


def get_font_height(font):
    return font.mvfont[1]


def get_font_char(font, c):
    for x,index in get_font_index(font):
        if x == c:
            d,m = divmod(font.mvfont[index+2]*font.mvfont[index+3], 8)
            chr_size = d+1 if m else d
            return font.mvfont[index:index+6+chr_size]

    return None


def get_font_ch_width(font, c):
    ch = get_font_char(font, c)
    if ch != None:
        return ch[5]
    else:
        return 0


def get_font_str_width(font, chars):
    w = 0
    for c in chars:
        w += get_font_ch_width(font, c)

    return w

def get_font_str_height(font, chars):
    h = 0
    for c in chars:
        char = get_font_char(font, c)
        if char == None:
            continue
        y_offset, width, height, x_offset, x_delta = ustruct.unpack('>5B', char[1:6])
        h = max(h, y_offset+height)
    return h

def font_decoder_nooff(znak):

    try:
        import struct
    except:
        import ustruct as struct

    y_offset, width, height, x_offset, x_delta = struct.unpack('>5B', znak[1:6])

    buf = [0]*(width*height)
    mask = 0x80
    dataptr = 5

    for j in range(height):
        for i in range(width):
            if not (( i + (j * width)) % 8 ):
                mask = 0x80
                dataptr += 1
            if znak[dataptr] & mask:
                bufPos = (j*width) + i 
                buf[bufPos] = 1
            mask = mask >> 1
    print(chr(znak[0]))
    for i in range(height):
        for j in range(width):
            print('##' if buf[i*width+j] else ' .', end='')
        print(' |{:02d}'.format(i))

def char_decoder_offset(char, fh=0):

    try:
        import struct
    except:
        import ustruct as struct

    y_offset, width, height, x_offset, x_delta = struct.unpack('>5B', char[1:6])

    mask = 0x80
    dataptr = 5

    for _ in range(y_offset*x_delta):
        yield False
    for j in range(height):
        for _ in range(x_offset):
            yield False
        for i in range(width):
            if not (( i + (j * width)) % 8 ):
                mask = 0x80
                dataptr += 1
            yield char[dataptr] & mask
            mask = mask >> 1
        for _ in range(x_delta - width - x_offset):
            yield False
    if y_offset + height < fh:
        for _ in range((fh - y_offset - height)*x_delta):
            yield False


def font_decoder_offset(znak, fonth = -1):

    try:
        import struct
    except:
        import ustruct as struct

    y_offset, width, height, x_offset, x_delta = struct.unpack('>5B', znak[1:6])

    th = max(height+y_offset, fonth)
    buf = bytearray(x_delta*th)

    pos = 0
    for p in char_decoder_offset(znak, fonth):
        buf[pos] = p
        pos += 1

    print('---')
    print('char: {}'.format(chr(znak[0])))
    print('   w: {:02},    h:{:02}'.format(width,height))
    print('xoff: {:02}, yoff:{:02}'.format(x_offset, y_offset))
    print('xdel: {:02}, fnth:{:02}'.format(x_delta, fonth))
    print()
    for j in range(x_delta):
        print('{:2x}'.format(j), end='')
    print()
    for i in range(th):
        for j in range(x_delta):
            blank = ' .'
            if i == fonth or i == height + y_offset:
                blank = '__'
            if i == y_offset:
                blank = '--'
            if j in [x_offset, x_offset+width]:
                blank = ' |'
            if j in [x_offset, x_offset+width] and i == y_offset:
                blank = '-+'
            if j in [x_offset, x_offset+width] and i == fonth:
                blank = '_|'
            print('##' if buf[i*x_delta+j] else blank, end='')
        print(' |{:02d}'.format(i))

